# README #

![prew1.png](http://i.imgur.com/5Ha00Va.png)

**
TwittParser(C#)** - a web-service that is used to parse twitter statuses for media content. Pet-project solely to test my knowledge and skills. 

**Technologies used:** 

*    C#, 
*    asp.net web api + owin + CORS, 
*    autofac, 
*    htmlagilitypack, 
*    Task API, 
*    Bootstrap, 
*    Angularjs.

### How do I get set up? ###

* Clone this repo. Open project in VS 2015-2013. Configure solution to run multiple projects. Set to run - API and UI.NancyClient.
* Configuration -none
* Project depends on .Net Framework 4.5.
* Database configuration - none.
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact