﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sample1.Classes
{
	public class ProcessResult<T>
	{
		public T Result { get; set; }
		public string Error { get; set; }
		public bool HasError { get { return string.IsNullOrEmpty(Error); } }
	}
}
