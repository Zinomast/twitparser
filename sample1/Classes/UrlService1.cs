﻿using sample1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sample1.Classes
{
	public class UrlService1 : IUrlService<Uri>
	{
		public Uri GetInfo(string source)
		{
			return new Uri(source);
		}

		public bool ValidateUrl(Uri url)
		{
			if (url.Host == "twitter.com" && url.Segments.Contains("status/"))
			{
				return true;
			}
			return false;
		}
		//public string Source { get { return url.OriginalString; } }
		//public Uri Url { get { return url; } }
		//private Uri url;
		//public UrlService1(string sourceUrl)
		//{
		//	this.url = new Uri(sourceUrl);
		//}

		//public bool CherkUrl()
		//{
		//	if (Url.Host == "twitter.com" && Url.Segments.Contains("status/"))
		//	{
		//		return true;
		//	}
		//	return false;
		//}
	}
}
