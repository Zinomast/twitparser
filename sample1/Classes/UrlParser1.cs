﻿using sample1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Extensions;

namespace sample1.Classes
{
	public class UrlParser1 : IParserService
	{
		public IConfiguration config;
		public IDocument document;
		public string result;
		public UrlParser1()
		{
			config = Configuration.Default.WithDefaultLoader();
		}
		public async Task GetContentAsync(string source)
		{
			document = await BrowsingContext.New(config).OpenAsync(source);
		}

		public async Task Parse()
		{
			result = await GetString();
		}

		private Task<string> GetString()
		{
			return Task.Factory.StartNew(() => document.QuerySelectorAll("data-aria-label-part").Select(value => value.GetAttribute("src")).FirstOrDefault()); 
		}
	}
}
