﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sample1.Interfaces
{
	public interface IParserService
	{
		Task GetContentAsync(string source);
		Task Parse();
	}
}
