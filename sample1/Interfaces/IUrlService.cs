﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sample1.Interfaces
{
	interface IUrlService<T> where T : class
	{
		T GetInfo(string source);
		bool ValidateUrl(T url);
	}
}
