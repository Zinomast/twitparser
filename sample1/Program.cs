﻿using sample1.Classes;
using sample1.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace sample1
{
	class Program
	{
		static void Main(string[] args)
		{
			string path = "https://twitter.com/dbwoqjq123/status/733697872605011968";
			var source = GetHtmlAsync(path);
			var parser = new UrlParser1();
			var document = source.Result;
			var task = parser.GetContentAsync(document);
			task.ContinueWith(m => parser.Parse());
			Task.WaitAll();
			Console.WriteLine(parser.result);
			Console.ReadLine();
		}
		private static Task<string> GetHtmlAsync(string urlString)
		{
			var client = new HttpClient();
			return client.GetStringAsync(urlString);
		}
	}
	//class Program
	//{
	//	static Regex regex = new Regex("<div>(.*)</div>", RegexOptions.Compiled);
	//	static string pattern = "";
	//	static void Main(string[] args)
	//	{
	//		var urlString = @"";
	//		if (urlString.IsValidPath())
	//		{
	//			var result = ProcessUrl(urlString).Result;
	//			if (result.HasError)
	//			{
	//				Console.WriteLine(result.Error);
	//			}
	//			else
	//			{
	//				Console.WriteLine(result.Result);
	//			}
	//		}
	//		else
	//		{
	//			Console.WriteLine("Invalid url");
	//		}
	//	}

	//	private static async Task<ProcessResult<string>> ProcessUrl(string urlString)
	//	{
	//		Task<string> getHtmlTask = GetHtmlAsync(urlString);
	//		string html = await getHtmlTask;
	//		var resultString = ParseHtml(html);
	//		return resultString;
	//	}

	//	private static ProcessResult<string> ParseHtml(string html)
	//	{
	//		var match = Regex.Matches(html, pattern);
	//		if (match.Count > 1)
	//		{
	//			throw new Exception();
	//		}
	//		if (match[0].Success)
	//		{
	//			var result = match[0].Groups[1].Captures[0].Value;
	//			return new ProcessResult<string>()
	//			{
	//				Result = result
	//			};
	//		}
	//		return new ProcessResult<string>()
	//		{
	//			Error = "No match"
	//		};
	//	}

	//	private static Task<string> GetHtmlAsync(string urlString)
	//	{
	//		var client = new HttpClient();
	//		return client.GetStringAsync(urlString);
	//	}
	//}
}
